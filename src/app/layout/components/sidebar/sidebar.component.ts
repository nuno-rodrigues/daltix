import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

import { DialogComponent } from '../dialog/dialog.component';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
    public showMenu: string;
    constructor(public dialog: MatDialog) {}

    ngOnInit() {
        this.showMenu = '';
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    openDialog(type: string): void {
      const dialogRef = this.dialog.open(DialogComponent, {
          width: '550px',
          data: {
            type: type
          },
          disableClose: false,
          autoFocus: false,
      });

      dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
      });

    }
}
