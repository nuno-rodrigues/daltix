import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, Sort } from '@angular/material';

import { AccountsService } from '../../../shared/services/accounts.service';

@Component({
    selector: 'app-tables',
    templateUrl: './tables.component.html',
    styleUrls: ['./tables.component.scss']
})

export class TablesComponent implements OnInit {
    displayedColumns = ['id', 'username', 'times_accessed', 'created'];
    dataSource: MatTableDataSource<UserData>;

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private appSettingsService: AccountsService) {
      const users: UserData[] = [];
      this.appSettingsService.getJSON().subscribe(data => {
        for (let i = 0; i <= data.length - 1; i++) {
          // console.log(data[i]);
          users.push(createNewUser(i, data[i]));
        }
        // Assign the data to the data source for the table to render
        this.dataSource = new MatTableDataSource(users);

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
    }

    ngOnInit() {
    }

}

export interface UserData {
    id: number;
    username: string;
    times_accessed: number;
    created: string;
    last_login: Date;
}

/** Builds and returns a new User. */
function createNewUser(id: number, data): UserData {
    return {
        id: id,
        username: data.username,
        times_accessed: data.times_accessed,
        created: data.created,
        last_login: data.last_login
    };
}
