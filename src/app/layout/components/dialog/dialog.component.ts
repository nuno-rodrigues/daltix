import { Component, Inject, OnInit, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
    selector: 'app-dialog',
    templateUrl: './dialog.component.html',
    styleUrls: ['./dialog.component.scss']
})

export class DialogComponent implements OnInit {

    constructor(
        public dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }

    @Input() title: string;
    @Input() content: string;

    ngOnInit() {
      this.setTitle();
    }

    setTitle(): void {
      switch (this.data.type) {
        case 'create': {
            this.title = 'Create User';
            break;
        }
        case 'edit': {
          this.title = 'Edit User';
          break;
        }
        case 'delete': {
          this.title = 'Delete User';
          break;
        }
        default: {
            this.title = 'Title';
            break;
        }
      }
    }

    checkContent(type) {
      if (this.data.type === type) {
        return true;
      } else {
        return false;
      }

    }

    checkIfAvaiable() {
      if (this.data.type === 'edit') {
        return true;
      } else {
        return false;
      }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onYesClick(): void {
      console.log('Check values before call services');
      console.log('Call add user service');
      this.dialogRef.close();
    }
}
