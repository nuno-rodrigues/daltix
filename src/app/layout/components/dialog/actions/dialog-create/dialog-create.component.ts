import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-dialog-create',
  templateUrl: './dialog-create.component.html',
  styleUrls: ['./dialog-create.component.scss']
})
export class DialogCreateComponent implements OnInit {

  hide = true;
  username  = new FormControl('', [Validators.required]);
  password  = new FormControl('', [Validators.required]);
  role      = new FormControl('', [Validators.required]);

  @Output() event = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  getUsernameErrorMessage() {
    return this.username.hasError('required') ? 'You must enter a username' : '';
  }

  getPasswordErrorMessage() {
    return this.password.hasError('required') ? 'You must enter a password' : '';
  }

  getRoleErrorMessage() {
    return this.role.hasError('required') ? 'You must choose an option' : '';
  }

}
