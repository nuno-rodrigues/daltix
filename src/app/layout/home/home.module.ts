import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import {
  MatCardModule,
  MatGridListModule,
  MatIconModule
} from '@angular/material';


import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { ChartsComponent } from '../components/charts/charts.component';
import { StatComponent } from '../components/stat/stat.component';

@NgModule({
    imports: [
      CommonModule,
      HomeRoutingModule,
      Ng2Charts,
      MatCardModule,
      MatGridListModule,
      MatIconModule,
      FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [
      HomeComponent,
      ChartsComponent,
      StatComponent
    ]
})

export class HomeModule {}
