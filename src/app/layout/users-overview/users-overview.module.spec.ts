import { UsersOverviewModule } from './users-overview.module';

describe('UsersOverviewModule', () => {
    let usersOverviewModule: UsersOverviewModule;

    beforeEach(() => {
        usersOverviewModule = new UsersOverviewModule();
    });

    it('should create an instance', () => {
        expect(UsersOverviewModule).toBeTruthy();
    });
});
