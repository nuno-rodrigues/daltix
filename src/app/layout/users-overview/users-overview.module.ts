import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatTableModule } from '@angular/material';
import { MatFormFieldModule, MatPaginatorModule } from '@angular/material';
import { MatInputModule } from '@angular/material';

import { UsersOverviewRoutingModule } from './users-overview-routing.module';
import { UsersOverviewComponent } from './users-overview.component';

import { TablesComponent } from '../components/tables/tables.component';

@NgModule({
    imports: [
      CommonModule,
      UsersOverviewRoutingModule,
      MatTableModule,
      MatFormFieldModule,
      MatPaginatorModule,
      MatInputModule,
    ],
    declarations: [
      UsersOverviewComponent,
      TablesComponent
    ]
})
export class UsersOverviewModule {}
