import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormControl } from '@angular/forms';

import {
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatDialogModule,
    MatSelectModule,
    MatFormFieldModule,
} from '@angular/material';

import { SidebarComponent } from './components/sidebar/sidebar.component';
import { TopnavComponent } from './components/topnav/topnav.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { DialogCreateComponent } from './components/dialog/actions/dialog-create/dialog-create.component';
import { DialogEditComponent } from './components/dialog/actions/dialog-edit/dialog-edit.component';
import { DialogDeleteComponent } from './components/dialog/actions/dialog-delete/dialog-delete.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        LayoutRoutingModule,
        MatToolbarModule,
        MatButtonModule,
        MatSidenavModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatSelectModule,
        MatFormFieldModule,
        MatListModule,
        MatDialogModule,
        FormsModule,
        ReactiveFormsModule,
        FlexLayoutModule.withConfig({addFlexToParent: false})
    ],
    declarations: [
      LayoutComponent,
      SidebarComponent,
      DialogComponent,
      TopnavComponent,
      DialogCreateComponent,
      DialogEditComponent,
      DialogDeleteComponent,
    ],
    exports: [
      DialogComponent,
      DialogCreateComponent,
      DialogEditComponent,
      DialogDeleteComponent
    ],
    entryComponents: [
      DialogComponent
    ]
})
export class LayoutModule {}
